name := "workshop-shared"

libraryDependencies ++= Seq(
  "io.circe" %%% "circe-core" % "0.10.0",
  "io.circe" %%% "circe-generic" % "0.10.0"
)
# Adopt a Pet

## Introduction

This repo hosts a workshop on ScalaJS for the PSUG of June 2019.

It uses scalajs-react by David Barry (Japgolly) for the front and the play
framework for the backend

## Prerequisites

In order to have everything working, you will need:

* Your favorite IDE supporting scala
* `sbt`, consider using [coursier](https://github.com/coursier/coursier) for better dependency management
* `npm`, consider using [nvm](https://github.com/creationix/nvm) to manage node versions in a better way
* [yarn](https://yarnpkg.com/en/docs/install)

## Starting the front application in dev mode

```
sbt fastOptJS::startWebpackDevServer ~fastOptJS
```

This command will will:
- Start the webpack dev server
- rebuild the scala part incrementally and use `hot` to reload the application

If you change the javascript dependencies, you will have to do a full rebuild of the js module.

## play-scalajs

`sbt new vmunier/play-scalajs.g8`

## TODO:

* pdf of the prez
* How-to start the app
package org.make.playscalajs.data

import org.make.workshop.shared.Pet
import org.make.workshop.shared.Race.Cat

object Cats {
  val felix = Pet(
    id = "cat-1",
    name = "Felix",
    race = Cat,
    age = 6,
    picture = "images/felix.jpeg",
    description = "Kitty cat with a cool description"
  )

  val minouchette = Pet(
    id = "cat-2",
    name = "Minouchette",
    race = Cat,
    age = 2,
    picture = "images/minouchette.jpg",
    description = "Awwwwwww"
  )

  val norminet = Pet(
    id = "cat-42",
    name = "Norminet",
    race = Cat,
    age = 42,
    picture =
      "images/norminet.jpg",
    description = "The answer."
  )

}

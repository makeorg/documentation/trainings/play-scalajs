package org.make.playscalajs.data

import org.make.workshop.shared.Pet

import org.make.workshop.shared.Race.Dog

object Dogs {
  val medor = Pet(
    id = "dog-1",
    name = "Medor",
    race = Dog,
    age = 14,
    picture =
      "images/medor.jpg",
    description = "Cool dog looking forward to sleeping with you."
  )

  val annivia = Pet(
    id = "dog-2",
    name = "Annivia",
    race = Dog,
    age = 2,
    picture =
      "images/annivia.jpg",
    description = "A bit shy but still a lovely dog."
  )

  val puppy = Pet(
    id = "dog-3",
    name = "Puppy",
    race = Dog,
    age = 0,
    picture =
      "images/puppy.jpg",
    description = "Happiness is a warm puppy."
  )
}

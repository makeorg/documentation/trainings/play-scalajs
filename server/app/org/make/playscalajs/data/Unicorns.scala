package org.make.playscalajs.data

import org.make.workshop.shared.Pet

import org.make.workshop.shared.Race.Unicorn

object Unicorns {
  val charlie = Pet(
    id = "unicorn-1",
    name = "Charlie",
    race = Unicorn,
    age = 521,
    picture = "images/charlie.png",
    description = "They took my freakin' kidney !"
  )

}

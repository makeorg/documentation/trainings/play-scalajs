package org.make.playscalajs.data

import org.make.workshop.shared.Pet

import org.make.workshop.shared.Race.Parrot

object Parrots {
  val coco = Pet(
    id = "parrot-1",
    name = "Coco",
    race = Parrot,
    age = 2,
    picture = "images/coco.png",
    description = "Coco ! Hello. Shhhht. Repeat !!"
  )

  val jack = Pet(
    id = "parrot-2",
    name = "Jack",
    race = Parrot,
    age = 6,
    picture = "images/jack.jpg",
    description = "Old one-eyed pirates' parrot."
  )
}

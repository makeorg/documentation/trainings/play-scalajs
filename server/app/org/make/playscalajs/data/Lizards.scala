package org.make.playscalajs.data

import org.make.workshop.shared.Pet

import org.make.workshop.shared.Race.Lizard

object Lizards {
  val balthazar = Pet(
    id = "lizard-1",
    name = "Balthazar",
    race = Lizard,
    age = 4,
    picture = "images/balthazar.jpeg",
    description = "He likes it hot and dry."
  )

}

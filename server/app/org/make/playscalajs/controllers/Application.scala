package org.make.playscalajs.controllers

import java.nio.file.{Files, Paths}

import akka.util.ByteString
import javax.inject._
import org.make.playscalajs.shared.SharedMessages
import play.api.http.HttpEntity
import play.api.mvc._

@Singleton
class Application @Inject()(cc: ControllerComponents)
    extends AbstractController(cc) {

  def index = Action {
    Ok(views.html.index(SharedMessages.itWorks))
  }

  def scalaJsAsset(file: String) = Action {
    Option(getClass.getResource(s"/public/images/$file")) match {
      case None => NotFound
      case Some(resource) =>
        Result(ResponseHeader(200),
               HttpEntity.Strict(ByteString(readFile(resource.getFile)), None))
    }
  }

  private def readFile(file: String): Array[Byte] = {
    Files.readAllBytes(Paths.get(file))
  }
}

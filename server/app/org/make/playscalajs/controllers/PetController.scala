package org.make.playscalajs.controllers

import javax.inject._
import org.make.playscalajs.data.Pets
import io.circe.syntax._
import play.api.mvc._

@Singleton
class PetController @Inject()(cc: ControllerComponents)
    extends AbstractController(cc) {

  def index(name: Option[String]) = Action {
    Ok(Pets.all.filter(p => name.forall(n => p.name.toLowerCase.contains(n.toLowerCase))).asJson.toString).as("application/json")
  }

  def single(id: String) = Action {
    Pets.all.find(_.id == id) match {
      case Some(pet) =>
        Ok(pet.asJson.toString).as("application/json")
      case None => NotFound
    }
  }

  def adopt(id: String) = Action {
    Pets.adopt(id)
    NoContent
  }
}

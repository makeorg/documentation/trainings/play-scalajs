name := "workshop-server"

Assets / pipelineStages := Seq(scalaJSPipeline)
pipelineStages := Seq(digest, gzip)

// triggers scalaJSPipeline when using compile or continuous compilation
Compile / compile := ((Compile / compile) dependsOn scalaJSPipeline).value

libraryDependencies ++= Seq(
  guice,
  specs2 % Test
)

enablePlugins(PlayScala, WebScalaJSBundlerPlugin)